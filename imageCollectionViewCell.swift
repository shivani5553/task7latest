//
//  imageCollectionViewCell.swift
//  task7
//
//  Created by SwordMac12 on 27/09/21.
//

import UIKit

class imageCollectionViewCell: UICollectionViewCell {
    static let identifier = "imageCollectionViewCell"
    private let imageView:UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    override init(frame:CGRect){
        super.init(frame: frame)
        contentView.addSubview(imageView)
    }
    required init ?(coder:NSCoder){
        fatalError()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.frame = contentView.bounds
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
    }
    func configure(with urlstring:String){
        guard let url = URL(string: urlstring)else {
            return
        }
        URLSession.shared.dataTask(with: url){[weak self]data, _,error in
            guard let data = data , error == nil else {
                return
            }
            DispatchQueue.main.async {
                let image = UIImage (data: data)
                self?.imageView.image = image
            }
        }
        .resume()
    }
}
